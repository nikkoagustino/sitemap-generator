<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Spatie\Sitemap\SitemapGenerator;
use Spatie\Sitemap\Tags\Url;

class APIController extends Controller
{
    
    function generateSitemap(Request $req){
        $path = 'sitemap.xml';
        SitemapGenerator::create('https://'.$req->target)
                        ->hasCrawled(function (Url $url) {
                           if ($url->segment(1) == '') {
                                $url->setPriority(1.0);
                           } else if (($url->segment(1) != '') && ($url->segment(2) == '')) {
                                $url->setPriority(0.8);
                           } else {
                                $url->setPriority(0.5);
                           }


                           return $url;
                        })
                        ->writeToFile($path);
        header('Content-Type: text/xml');
        echo file_get_contents($path);
    }
}
